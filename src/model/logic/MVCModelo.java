package model.logic;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.Reader;

import model.data_structures.ArregloDinamico;
import model.data_structures.IArregloDinamico;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Viaje;

/**
 * Definicion del modelo del mundo
 *
 */

public class MVCModelo {
	/**
	 * Atributos del modelo del mundo
	 */
	private static final String PATH1 = "./bogota-cadastral-2018-1-All-MonthlyAggregate.csv";
	private static final String PATH2 = "./bogota-cadastral-2018-2-All-MonthlyAggregate.csv";
	private LinkedList<Viaje> viajes;
	
	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo() throws IOException
	{
		viajes = new LinkedList<Viaje>();
		try
		(
			Reader reader = Files.newBufferedReader(Paths.get(PATH1));
			CSVReader csv = new CSVReader(reader);
		){
			String[] next;
			while((next = csv.readNext()) != null)
			{
				viajes.add(new Viaje(Integer.parseInt(next[0]),Integer.parseInt(next[1]),Integer.parseInt(next[2]),Double.parseDouble(next[3]),Double.parseDouble(next[4]),Double.parseDouble(next[5]),Double.parseDouble(next[6])));
			}
		}
		try
		(
			Reader reader = Files.newBufferedReader(Paths.get(PATH2));
			CSVReader csv = new CSVReader(reader);
		){
			String[] next;
			while((next = csv.readNext()) != null)
			{
				viajes.add(new Viaje(Integer.parseInt(next[0]),Integer.parseInt(next[1]),Integer.parseInt(next[2]),Double.parseDouble(next[3]),Double.parseDouble(next[4]),Double.parseDouble(next[5]),Double.parseDouble(next[6])));
			}
		}
	}
	
	/**
	 * Servicio de consulta de numero de elementos presentes en el modelo 
	 * @return numero de elementos presentes en el modelo
	 */
	public int darTamano()
	{
		return viajes.size;
	}

	/**
	 * Requerimiento de agregar dato
	 * @param dato
	 */
	public void agregar(Viaje j)
	{	
		viajes.add(j);
	}
	public ArrayList<Viaje> getTripsFromDestination(int id, int mes)
	{
		ArrayList<Viaje> returner = new ArrayList<Viaje>();
		Node<Viaje> current = viajes.getNode(0);
		while(current.hasNext())
		{
			if(current.getData().getSourceid() == id && current.getData().getMonth() == mes)
			{
				returner.add(current.getData());
			}
			current = current.getNext();
		}
		return returner;
	}



}
