package model.data_structures;

public class LinkedList<T> 
{
	private Node<T> first;
	public int size = 0;
	public LinkedList()
	{
	}
	public void add(T element)
	{
		Node<T> node = new Node<T>(element);
		if(first == null)
			first = node;
		else
		{
			Node<T> current = first;
			while(current.hasNext())
			{
				current = current.getNext();
			}
			current.setNext(node);
		}
		size++;
		
	}
	public T get(int index)
	{
		Node<T> current = first;
		int j = 0;
		while(j < index)
		{
			current = current.getNext();
			j++;
		}
		return current.getData();
	}
	public Node<T> getNode(int index)
	{
		Node<T> current = first;
		int j = 0;
		while(j < index)
		{
			current = current.getNext();
			j++;
		}
		return current;
	}
	public void remove(int index)
	{
		if(getNode(index)== null)
			return;
		if(index == 0)
		{
			if(first.getNext() == null)
				first = null;
			else
			{
				first = first.getNext();
			}
		}
		else
		{
			getNode(index--).setNext(getNode(index).getNext());
		}
		size--;
	}

}
