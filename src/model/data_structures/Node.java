package model.data_structures;

public class Node<T>
{
	private T data;
	private Node<T> next;
	
	public Node(T iData)
	{
		data = iData;
	}
	public void setNext( Node<T> iNext)
	{
		next = iNext;
	}
	public Node<T> getNext()
	{
		return next;
	}
	public T getData()
	{
		return data;
	}
	public boolean hasNext()
	{
		if(next != null)
			return true;
		return false;
	}

}
