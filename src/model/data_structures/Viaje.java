package model.data_structures;

public class Viaje {
	private int sourceid;
	private int dstid;
	private int month;
	private double mean_travel_time;
	private double standard_deviation_travel_time;
	private double geometric_mean_travel_time;
	private double geometric_standard_deviation_travel_time;
	
	public Viaje(int iSourceid, int iDstid, int iMonth, double iTT, double sdTT, double gmTT, double gsdTT)
	{
		sourceid = iSourceid;
		dstid = iDstid;
		month = iMonth;
		mean_travel_time = iTT;
		standard_deviation_travel_time = sdTT;
		geometric_mean_travel_time = gmTT;
		geometric_standard_deviation_travel_time = gsdTT;
	}
	
	public int getSourceid()
	{
		return sourceid;
	}
	public int getDstid()
	{
		return dstid;
	}
	public int getMonth()
	{
		return month;
	}
	public double getMeanTravelTime()
	{
		return mean_travel_time;
	}
	public double getStandardDeviationTravelTime()
	{
		return standard_deviation_travel_time;
	}
	public double getGeometricMeanTravelTime()
	{
		return geometric_mean_travel_time;
	}
	public double getGeometricStandardDeviationTravelTime()
	{
		return geometric_standard_deviation_travel_time;
	}
	
	@Override
	public String toString()
	{
		return "Zona origen:" + sourceid + " Zona Destino:" + dstid + " Tiempo Promedio:" + mean_travel_time + " Desviacion Estandar:" + standard_deviation_travel_time ;
	}

}
