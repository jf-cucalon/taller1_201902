package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.Viaje;
import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 * @throws IOException 
	 */
	public Controller () throws IOException
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() throws IOException 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		int id = 0;
		int mes = 0;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargando Datos");
				    int capacidad = lector.nextInt();
				    modelo = new MVCModelo(); 
					System.out.println("Datos Cargados");
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 2:
					System.out.println("--------- \nDar enteros de numero de viaje y mes ");
					id = lector.nextInt();
					mes = lector.nextInt();
					System.out.println("Datos agregado");
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

				case 3:
					System.out.println("--------- \nLos viajes son:");
					ArrayList<Viaje> respuesta = modelo.getTripsFromDestination(id, mes);
					if ( respuesta != null)
					{
						System.out.println("Datos encontrado: "+ respuesta);
					}
					else
					{
						System.out.println("Dato NO encontrado");
					}
					System.out.println("Numero actual de elementos " + modelo.darTamano() + "\n---------");						
					break;

					
				case 6: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;	

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
